{
  "data": [
    {
      "group": "group name 1",
      "summary": {
       "gross": 1496.67,
       "orders": 17,
       "itemqty":  23,
       "avgprice": 88.03,
       "tax": 34.09
      },
     "details": [
      {
         "itemcode": "123a",
         "itemname": "Item name 1",
         "department": "Department name",
         "itemgroup": "item group 1",
         "pricelevel": "price level 1",
         "qty": 22,
         "avgprice": 87.98,
         "totalprice": 1296.14
      },
      {
         "itemcode": "132b",
         "itemname": "Item name 2",
         "department": "Department name",
         "itemgroup": "item group 1",
         "pricelevel": "price level 4",
         "qty": 1,
         "avgprice": 88.75,
         "totalprice": 200.53
      }
     ]
    },
    {
      "group": "group name 2",
      "summary": {
       "gross": 7831.56,
       "orders": 120,
       "itemqty": 178,
       "avgprice": 65.26,
       "tax": 98.06
      },
     "details": [
      {
         "itemcode": "123a",
         "itemname": "Item name 1",
         "department": "Department name",
         "itemgroup": "item group 2",
         "pricelevel": "price level 1",
         "qty": 152,
         "avgprice": 62.98,
         "totalprice": 1296.14
      },
      {
         "itemcode": "132b",
         "itemname": "Item name 2",
         "department": "Department name",
         "itemgroup": "item group 2",
         "pricelevel": "price level 4",
         "qty": 26,
         "avgprice": 88.75,
         "totalprice": 6570.23
      }
     ]
    }
  ]
}
