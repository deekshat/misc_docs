{
   "data": [
   {
      "itemcode": "123a",
      "itemname": "Item name 1",
      "department": "Department name",
      "itemgroup": "item group 1",
      "pricelevel": "price level 1",
      "qty": 22,
      "avgprice": 87.98,
      "totalprice": 1296.14
   },
   {
      "itemcode": "132b",
      "itemname": "Item name 2",
      "department": "Department name",
      "itemgroup": "item group 1",
      "pricelevel": "price level 4",
      "qty": 1,
      "avgprice": 88.75,
      "totalprice": 200.53
   }
   ]
}
